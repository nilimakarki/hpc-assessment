#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <errno.h>
#include <sys/stat.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <math.h>
#include <crypt.h>
#include <mpi.h>
/***********************************************************************
*******
  Demonstrates how to crack an encrypted password using a simple
  "brute force" algorithm. Works on passwords that consist only of 2
   uppercase letters and a 2 digit integer.
   
   This is the MPI version of password cracker in which work is shared betwwen 
   two compute instances where instance 1 should explore AA through to MZ, whilst 
   instance 2 should explore NA through to ZZ 

  Compile with:
    cc -o mpi-passwordCrack mpi-passwordCrack.c -lcrypt

  
  Run and redirect:
    ./mpi-passwordCrack > mpi-passwordCrack-result.txt

************************************************************************
******/
int n_passwords = 4;
// encrypted passwords are NI12, BI00, SU20, NA18
char *encrypted_passwords[] = {

"$6$KB$G3zla77yQUJ.6xTwWUO2D5fPBAhCi3M3Fs7uls5MUR7x/f.7uTwW8zsedSIwm6M/RTl1YGwJONtYo4ygaQhmv1",

"$6$KB$.iQCYbDV8IezRKOtaookYRummYT6duJBPFvMQRfyqGFb/v/.hMljF2UDdVF7gugKLVeYWazlJZwjGz6UUuPPI/",

"$6$KB$4DorzrVkydzyAIpe3seObGhn.lqP7K4AZQQplk74izVyPygu5aq5ZjhM.l1wduduxIoabyD9ev1RCR6ZeUH13/",

"$6$KB$mkMx8Qd6VSvHRMKMm55qxbOhNdEstrOexcTiVp0Hj72pYRq4N6FbhclKrcjviwgdWl/k.nn/Cq5ru6fBHtQ4L0"
};

void crack(char *salt_and_encrypted, char start, char finish);

/**
 Required by lack of standard function in C.   
*/
void substr(char *dest, char *src, int start, int length){
  memcpy(dest, src + start, length);
  *(dest + length) = '\0';
}

/**
 This function can crack the kind of password explained above. All
combinations
 that are tried are displayed and when the password is found, #, is put
at the 
 start of the line. Note that one of the most time consuming operations
that 
 it performs is the output of intermediate results, so performance
experiments 
 for this kind of program should not include this. i.e. comment out the
printfs.
*/

void crack(char *salt_and_encrypted, char start, char finish){
  int x, y, z;     // Loop counters
  char salt[7];    // String used in hashing the password. Need space for \0
  char plain[7];   // The combination of letters currently being checked
  char *enc;       // Pointer to the encrypted password
  int count = 0;   // The number of combinations explored so far

  substr(salt, salt_and_encrypted, 0, 6);
  for(x= start; x<= finish; x++){
    for(y='A'; y<='Z'; y++){
      for(z=0; z<=99; z++){
        sprintf(plain, "%c%c%02d",x, y, z); 
        enc = (char *) crypt(plain, salt);
        count++;
        if(strcmp(salt_and_encrypted, enc) == 0){
          printf("#%-8d%s %s\n", count, plain, enc);
        } else {
          printf(" %-8d%s %s\n", count, plain, enc);
        }
      }
    }
}
  printf("%d solutions explored\n", count);
}

// Calculate the difference between two times. Returns zero on
// success and the time difference through an argument. It will 
// be unsuccessful if the start time is after the end time.
int time_difference(struct timespec *start, struct timespec *finish, 
                              long long int *difference) {
  long long int ds =  finish->tv_sec - start->tv_sec; 
  long long int dn =  finish->tv_nsec - start->tv_nsec; 

  if(dn < 0 ) {
    ds--;
    dn += 1000000000; 
  } 
  *difference = ds * 1000000000 + dn;
  return !(*difference > 0);
}

int main(int argc, char **argv)
{
	int size, rank;
	struct timespec start, finish;
	long long int time_elapsed;
	MPI_Init(NULL, NULL);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	if(size != 3)
	{
		if(rank == 0)
			{
			printf("This program needs exactly 3 process \n");
			exit(-1); 			
			}
	}
	else
		{
			for(int i=0; i<n_passwords; i++)
				{
				if(rank==0){
					clock_gettime(CLOCK_MONOTONIC, &start);
					MPI_Send(&encrypted_passwords[i][0],512,MPI_BYTE,1,0,MPI_COMM_WORLD);
					MPI_Send(&encrypted_passwords[i][0],512,MPI_BYTE,2,0,MPI_COMM_WORLD);
					}
				else
					{
					if(rank==1){
						char password[512];
						MPI_Status status;
						MPI_Recv(&password[0], 512,MPI_BYTE,0,0,MPI_COMM_WORLD,&status);
						crack(password, 'A', 'M');			
						}
					if(rank==2){
						char password[512];
						MPI_Status status;
						MPI_Recv(&password[0], 512,MPI_BYTE,0,0,MPI_COMM_WORLD,&status);
						crack(password, 'N', 'Z');			
						}	
					}
				}
		}
	MPI_Barrier(MPI_COMM_WORLD);
		if(rank==0)
			{
				clock_gettime(CLOCK_MONOTONIC, &finish);
  				time_difference(&start, &finish, &time_elapsed);
  				printf("Time elapsed was %lldns or %0.9lfs or %0.9lfmin\n", time_elapsed, 
                                         (time_elapsed/1.0e9), ((time_elapsed/1.0e9)/60));
			}
		MPI_Finalize();
return 0;
}

					



