#include <stdio.h>
#include <cuda_runtime_api.h>
#include <time.h>
/****************************************************************************
  This is the CUDA version of of the two-initials-four-digits password cracker.
  It uses separate CUDA thread to process each possible pair of initials, 
  i.e. thread 0 processes AA through to thread 675 processing ZZ. 
  Each thread therefore performs the 10000 iterations that explore 
  the four digit integers.

  
  The intentions of this program are:
    1) Demonstrate the use of __device__ and __global__ functions
    2) Enable a simulation of password cracking in the absence of library 
       with equivalent functionality to libcrypt. The password to be found
       is hardcoded into a function called is_a_match.   

  Compile:
    nvcc -o cuda-passwordCracking cuda-passwordCracking.cu
  Run:
    ./cuda-passwordCracking
  Redirect result into text file:
    ./cuda-passwordCracking > cuda-passwordCracking-result.txt
   
  Dr Kevan Buckley, University of Wolverhampton, 2018
*****************************************************************************/

/****************************************************************************
  This function returns 1 if the attempt at cracking the password is 
  identical to the plain text password string stored in the program. 
  Otherwise,it returns 0.
*****************************************************************************/

__device__ int match_found(char *attempt) {
  char plainPassword1[] = "NI0010";
  char plainPassword2[] = "BI1101";
  char plainPassword3[] = "SU2216";
  char plainPassword4[] = "NA8859";


  char *a = attempt;
  char *b = attempt;
  char *c = attempt;
  char *d = attempt;
  char *p1 = plainPassword1;
  char *p2 = plainPassword2;
  char *p3 = plainPassword3;
  char *p4 = plainPassword4;

  while(*a == *p1) { 
   if(*a == '\0') 
    {
	printf("Password found: %s\n",plainPassword1);
      break;
    }

    a++;
    p1++;
  }
	
  while(*b == *p2) { 
   if(*b == '\0') 
    {
	printf("Password found: %s\n",plainPassword2);
      break;
    }

    b++;
    p2++;
  }

  while(*c == *p3) { 
   if(*c == '\0') 
    {
	printf("Password found: %s\n",plainPassword3);
      break;
    }

    c++;
    p3++;
  }

  while(*d == *p4) { 
   if(*d == '\0') 
    {
	printf("Password found: %s\n",plainPassword4);
      return 1;
    }

    d++;
    p4++;
  }
  return 0;

}


/****************************************************************************
  The kernel function assume that there will be only one thread and uses 
  nested loops to generate all possible passwords and test whether they match
  the hidden password.
*****************************************************************************/

__global__ void  kernel() {
char p,q,r,s;
  
  char password[7];
  password[6] = '\0';

  int a = blockIdx.x+65;
  int c = threadIdx.x+65;
  char value1 = a; 
  char value2 = c; 
    
  password[0] = value1;
  password[1] = value2;
	for(p='0'; p<='9'; p++){
	  for(q='0'; q<='9'; q++){
	   for(r='0'; r<='9'; r++){
	     for(s='0'; s<='9'; s++){
	        password[2] = p;
	        password[3] = q;
	        password[4] = r;
	        password[5] = s; 
	      if(match_found(password)) {
		//printf("Success");
	      } 
             else {
	     //printf("tried: %s\n", password);		  
	         }
	      }
	   }
	}
    }
}

// Calculate the difference between two times. Returns zero on
// success and the time difference through an argument. It will 
// be unsuccessful if the start time is after the end time.

int time_difference(struct timespec *start, struct timespec *finish, long long int *difference) {
  long long int ds =  finish->tv_sec - start->tv_sec; 
  long long int dn =  finish->tv_nsec - start->tv_nsec; 

  if(dn < 0 ) {
    ds--;
    dn += 1000000000; 
  } 
  *difference = ds * 1000000000 + dn;
  return !(*difference > 0);
}


int main() {

  struct  timespec start, finish;
  long long int time_elapsed;
  clock_gettime(CLOCK_MONOTONIC, &start);

  kernel <<<26,26>>>();
  cudaThreadSynchronize();

  clock_gettime(CLOCK_MONOTONIC, &finish);
  time_difference(&start, &finish, &time_elapsed);
  printf("Time elapsed was %lldns or %0.9lfs or %0.9lfmin\n", time_elapsed,(time_elapsed/1.0e9),   
         (time_elapsed/1.0e9/60)); 

  return 0;
}

