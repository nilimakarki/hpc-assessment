#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <crypt.h>
#include <time.h>

/***********************************************************************
*******
  Demonstrates how to crack an encrypted password using a simple
  "brute force" algorithm. Works on passwords that consist only of 3 
  upercase letters and a 2 digit integer.

  Compile with:
    cc -o 3initial-CrackAZ99-With-Data 3initial-CrackAZ99-With-Data.c -lcrypt

  Run with:
    ./3initial-CrackAZ99-With-Data 

  To redirect result into text file:
    ./3initial-CrackAZ99-With-Data > 3initial-result.txt

************************************************************************
******/
int n_passwords = 4;

// encypted passwords are NJK88, SUP09, NIL22, BIN73

char *encrypted_passwords[] = {

"$6$KB$Kxy4a.EmU3zophZhtHWgHLgj2UM01fSm0a0Lon3WYShi4jMAsiy1NpFl5MGyQPSnXKHoVXRBd8QsybATqXuM/0",

"$6$KB$7ynM6Bs8nza0HthRAtXQEco4Ko6uN3ur9rJZkVToIG2Vt4FBGZR8/HgiFaIndnUrfV5aA4bq36WEkahv.0lXX1",

"$6$KB$ZYWCC4Kb9EzpG.VDTf8ER3wvBpI3.cBHvyBMxKuOqiG1W2aizRp0/Z7CBHCZ4jSQa8nBtXFz0SkBeqcByFTNQ0",

"$6$KB$oLxAvimBRcFs9bGEqpYQPlwiUiFxt8awY6dbnYFkDVvSM5JCjjRbhfUltONONa3YH1qXuONQV8cZ/FrSpr2DP0"
};

/**
 Required by lack of standard function in C.   
*/

void substr(char *dest, char *src, int start, int length){
  memcpy(dest, src + start, length);
  *(dest + length) = '\0';
}

/**
 This function can crack the kind of password explained above. All
combinations
 that are tried are displayed and when the password is found, #, is put
at the
 start of the line. Note that one of the most time consuming operations
that
 it performs is the output of intermediate results, so performance
experiments
 for this kind of program should not include this. i.e. comment out the
printfs.
*/

void crack(char *salt_and_encrypted){
  int x, y, z;     // Loop counters
  char salt[7];    // String used in hashing the password. Need spacefor \0
  char plain[7];   // The combination of letters currently being checked
  char *enc;       // Pointer to the encrypted password
  int count = 0;   // The number of combinations explored so far

  substr(salt, salt_and_encrypted, 0, 6);

  for(x='A'; x<='Z'; x++){
    for(y='A'; y<='Z'; y++){
      for(s='A'; s<='Z'; s++){
        for(z=0; z<=99; z++){
         sprintf(plain, "%c%c%c%02d", x, y, s,z);
         enc = (char *) crypt(plain, salt);
         count++;
         if(strcmp(salt_and_encrypted, enc) == 0){
           printf("#%-8d%s %s\n", count, plain, enc);
         } else {
           printf(" %-8d%s %s\n", count, plain, enc);
         }
        }
      }
    }
  }
  printf("%d solutions explored\n", count);
}
// Calculate the difference between two times. Returns zero on
// success and the time difference through an argument. It will 
// be unsuccessful if the start time is after the end time.
int time_difference(struct timespec *start, struct timespec *finish, 
                              long long int *difference) {
  long long int ds =  finish->tv_sec - start->tv_sec; 
  long long int dn =  finish->tv_nsec - start->tv_nsec; 

  if(dn < 0 ) {
    ds--;
    dn += 1000000000; 
  } 
  *difference = ds * 1000000000 + dn;
  return !(*difference > 0);
}

int main(int argc, char *argv[]){
  int i;
  struct timespec start, finish;   
  long long int time_elapsed;

  clock_gettime(CLOCK_MONOTONIC, &start);

  for(i=0;i<n_passwords;i<i++) {
    crack(encrypted_passwords[i]);
  }

  clock_gettime(CLOCK_MONOTONIC, &finish);
  time_difference(&start, &finish, &time_elapsed);
  printf("Time elapsed was %lldns or %0.9lfs or %0.9lfmin\n", time_elapsed, (time_elapsed/1.0e9), ((time_elapsed/1.0e9)/60)); 


  return 0;
}


