#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <crypt.h>
#include <ctype.h>
#include <time.h>

/***********************************************************************
*******
  Demonstrates how to crack an encrypted password using a simple
  "brute force" algorithm. This is the multithread version of passowrd cracking.
  Works on passwords that consist only of 2 uppercase letters and a 2 digit integer.

  Compile with:
    cc -o multithread-passwordCracking multithread-passwordCracking.c -lcrypt -pthread

  Run with:
    ./multithread-passwordCracking

  To redirect result into text file:
    ./multithread-passwordCracking > multithread-passwordCracking-result.txt

************************************************************************
******/
int n_passwords = 4;
// two threads are created
pthread_t thread_1, thread_2;

char *encrypted_passwords[] = {

"$6$KB$GBVaKSTtLZW0JdGUtA3Not7zfm.y3TVu4794tItHazIldslHZ8jyaNfulL2j3lfwZ6UJ5quOjm7S1PIPgcR9Z/",

"$6$KB$hZ.CC7zI1bt9cPr7Z4cdMH0Hc7pvWb4NywkydXlUM3GWSGaQQAWA7qkHoiXLtH.xusB9kFs0NHZWl3kFxfgQi/",

"$6$KB$J3m7A6YF43jN.rIbG8Awh6mNUw7WArtgZ1qN39ijK8NWs0.0dpFmD0RRktXQLqrBJMV3z3BYCt24JnG23bfGy.",

"$6$KB$d5TFYVYcXhnzUaB3o.JQ0MytgBkcwb66OTtAmt/PgjJMmEqH4QjKBlrVHIzTuxAMJEzO/h7ASM.xQxlhBio330"
};

/**
 Required by lack of standard function in C.   
*/


void substr(char *dest, char *src, int start, int length){
  memcpy(dest, src + start, length);
  *(dest + length) = '\0';
}

/**
 These two functions can crack the kind of password explained above. All
 combinations that are tried are displayed and when the password is found, #, is put
 at the start of the line. Note that one of the most time consuming operations
 that it performs is the output of intermediate results, so performance experiments
 for this kind of program should not include this. i.e. comment out theprintfs.
*/

//this function search for passwords starting from A-M
void *kernel_function_1(void *salt_and_encrypted){

  int x, y, z;     // Loop counters
  char salt[7];    // String used in hashing the password. Need space
  char plain[7];   // The combination of letters currently being checked
  char *enc;       // Pointer to the encrypted password
  int count = 0;   // The number of combinations explored so far
  substr(salt, salt_and_encrypted, 0, 6);
	for(x='A'; x<='M'; x++){
	    for(y='A'; y<='Z'; y++){
	      for(z=0; z<=99; z++){
		sprintf(plain, "%c%c%02d",x, y, z);
		enc = (char *) crypt(plain, salt);
		count++;
		if(strcmp(salt_and_encrypted, enc) == 0){
		  printf("#%-8d%s %s\n", count, plain, enc);
		} else {
		  printf(" %-8d%s %s\n", count, plain, enc);
		}
	      }
	    }
	  }
  printf("%d solutions explored\n", count);

}

//this function search for passwords starting from N-Z
void *kernel_function_2(void *salt_and_encrypted){

  int x, y, z;     // Loop counters
  char salt[7];    // String used in hashing the password. Need space
  char plain[7];   // The combination of letters currently being checked
  char *enc;       // Pointer to the encrypted password
  int count = 0;   // The number of combinations explored so far
  substr(salt, salt_and_encrypted, 0, 6);
	for(x='N'; x<='Z'; x++){
	    for(y='A'; y<='Z'; y++){
	      for(z=0; z<=99; z++){
		sprintf(plain, "%c%c%02d", x, y, z);
		enc = (char *) crypt(plain, salt);
		count++;
		if(strcmp(salt_and_encrypted, enc) == 0){
		  printf("#%-8d%s %s\n", count, plain, enc);
		} else {
		  printf(" %-8d%s %s\n", count, plain, enc);
		}
	      }
	    }
	  }
  printf("%d solutions explored\n", count);

}

// Calculate the difference between two times. Returns zero on
// success and the time difference through an argument. It will 
// be unsuccessful if the start time is after the end time.
int time_difference(struct timespec *start, struct timespec *finish, long long int *difference) {
  long long int ds =  finish->tv_sec - start->tv_sec; 
  long long int dn =  finish->tv_nsec - start->tv_nsec; 

  if(dn < 0 ) {
    ds--;
    dn += 1000000000; 
  } 
  *difference = ds * 1000000000 + dn;
  return !(*difference > 0);
}


int main(int argc, char *argv[]){
 
  int i;
  struct timespec start, finish;
  long long int time_elapsed;   
  clock_gettime(CLOCK_MONOTONIC, &start);

  for(i=0;i<n_passwords;i<i++) {
   pthread_create(&thread_1, NULL, kernel_function_1, encrypted_passwords[i]);// thread_1 calls kernel_function_1
   pthread_create(&thread_2, NULL, kernel_function_2, encrypted_passwords[i]);// thread_2 calls kernel_function_2 
   pthread_join(thread_1, NULL);
   pthread_join(thread_2, NULL);		

  }

  clock_gettime(CLOCK_MONOTONIC, &finish);
  time_difference(&start, &finish, &time_elapsed);
  printf("Time elapsed was %lldns or %0.9lfs or %0.9lfmin\n", time_elapsed, (time_elapsed/1.0e9), ((time_elapsed/1.0e9)/60));


  return 0;
}
